# 7-8TV
![](https://github.com/nu11secur1ty/7-8TV/blob/master/wall/78.jpg)
- Directly download and watching on VLC for Linux
```bash
wget https://raw.githubusercontent.com/nu11secur1ty/7-8TV/master/78.m3u8 && vlc 78.m3u8 & disown
```
***For Windows USERS***

- Copy and paste this URL link into your browser, save the file, unzip it, and play with VLC player ;)
- Копирате линка по-долу в адресната линия на вашия браузър разархивирате файла и го стартирате с VLC плеар.

```url
https://github.com/nu11secur1ty/7-8TV/blob/master/windows/78.zip?raw=true
```
